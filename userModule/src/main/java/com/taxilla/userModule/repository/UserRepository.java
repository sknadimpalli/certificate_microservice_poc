package com.taxilla.userModule.repository;

import com.taxilla.userModule.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User,Long> {

    User getUserDetailsByUserId(Long id);
}
