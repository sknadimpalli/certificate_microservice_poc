package com.taxilla.userModule.controller;

import com.taxilla.userModule.entity.User;
import com.taxilla.userModule.service.UserService;
import com.taxilla.userModule.vo.ResponseVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService service;

    @PostMapping("/")
    public User saveUser(@RequestBody User user){
        log.info("Inside of SaveUser method in ",this.getClass().getName());
        return service.saveUser(user);
    }

    @GetMapping("/{id}")
    public ResponseVo getUserById(@PathVariable("id") Long id){
        log.info("Inside of getUserById method in ",this.getClass().getName());
        return service.getUserById(id);
    }
}
