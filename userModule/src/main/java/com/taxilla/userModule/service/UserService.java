package com.taxilla.userModule.service;

import com.taxilla.userModule.entity.User;
import com.taxilla.userModule.repository.UserRepository;
import com.taxilla.userModule.vo.Department;
import com.taxilla.userModule.vo.ResponseVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;

@Service
@Slf4j
public class UserService {

    @Autowired
    private UserRepository repository;

    @Autowired
    private RestTemplate restTemplate;

    public User saveUser(User user) {
        log.info("Inside of SaveUser method in ",this.getClass().getName());
        return repository.save(user);
    }

    public ResponseVo getUserById(Long id) {
        log.info("Inside of getUserById method in ",this.getClass().getName());
            ResponseVo responseVo= new ResponseVo();
            User user = repository.getUserDetailsByUserId(id);
            log.info("user details : ",user.toString());
            log.info("Department Id:  ",user.getDepartmentId());
        String url = "https://localhost:9001/department/"+user.getDepartmentId();
//            System.out.println("url :: "+url);
//            String url = "https://pocdept.taxilla.com:9001/department/"+user.getDepartmentId();
//        String url = "https://DEPARTMENT-SERVICE/department/"+user.getDepartmentId();

//        String url = "https://localhost:9001/department/"+user.getDepartmentId();
        System.out.println("url :: "+url);
//            URI uri = URI.create(System.getProperty("department.service"));
//            restTemplate.put(uri, id);
//            URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
//                    .buildAndExpand(user.getDepartmentId()).toUri();
//            ResponseEntity.created(location).build();
        Department department = restTemplate.getForObject(url,Department.class);
        responseVo.setUser(user);
        responseVo.setDepartment(department);
        return responseVo;
    }
}
