package com.taxilla.userModule.vo;

import com.taxilla.userModule.entity.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResponseVo {
    private User user;
    private Department department;
}
