package com.taxilla.department.controller;

import com.taxilla.department.entity.Department;
import com.taxilla.department.service.DepartmentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.POST;

@RestController
@Slf4j
@RequestMapping("/department")
public class DepartmentController {

    @Autowired
    private DepartmentService service;

    @PostMapping("/")
    public Department saveDepartmentDetails(@RequestBody Department department){
        log.info("Inside of SaveDepartmentDetails Method in ",this.getClass().getName());
        return service.saveDepartmentDetails(department);
    }

    @GetMapping("/{id}")
    public Department getDepartmentDetailsById(@PathVariable("id") Long id){
        log.info("Inside of getDepartmentDetailsById Method in ",this.getClass().getName());
        return service.getDepartmentDetailsById(id);
    }
}
