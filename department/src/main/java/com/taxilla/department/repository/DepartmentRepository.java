package com.taxilla.department.repository;

import com.taxilla.department.entity.Department;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DepartmentRepository extends JpaRepository<Department,Long> {
    Department findDepartmentDetailsById(Long id);
}
