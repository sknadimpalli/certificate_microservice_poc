package com.taxilla.department.service;

import com.taxilla.department.entity.Department;
import com.taxilla.department.repository.DepartmentRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class DepartmentService {

    @Autowired
    private DepartmentRepository repository;


    public Department saveDepartmentDetails(Department department) {
        log.info("Inside of saveDepartmentDetails Method in ",this.getClass().getName());
        return repository.save(department);
    }

    public Department getDepartmentDetailsById(Long departmentId) {
        log.info("Inside of getDepartmentDetailsById Method in ",this.getClass().getName());
        return repository.findDepartmentDetailsById(departmentId);
    }
}
